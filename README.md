#README file

#Functionality to calculate browser fingerprint
#It's using browser plugins, timezone, language, fonts, user agent itself, screen resolution and color depth, availability of local storage and session storage

#MD5 function is taken from http://www.myersdaily.org/joseph/javascript/md5-text.html
#fonts are taken from flash file compiled from FontList.as.

#To calculate fingerprint hash run
var fphash = Fingerprint.getHash();

#To calculate raw fingerprint run
var fpraw = Fingerprint.getRaw();

