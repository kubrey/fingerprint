/**
 * 
 * Object calculating browser fingerpint
 * @author <kubrey@gmail.com>
 * @type {Object}
 */
var Fingerprint = {
    private: {
        plugins: '',
        screen: null,
        timezone: null,
        lang: null,
        ua: null,
        storage: null,
        fonts: null
    },
    data: null,
    setPlugins: function() {
        var plugins = navigator.plugins;
        if (plugins !== undefined) {
            var it = 0;
            var pl = new Array();
            for (var iter in plugins) {
                if (plugins[iter]['name'] !== undefined) {
                    pl[it] = plugins[iter]['name'] + '-' + plugins[iter]['description'];
                    it++;
                }
            }
            for (var iter2 in pl) {
                Fingerprint.private.plugins += pl[iter2] + ":::";
            }
        }
    },
    setStorage: function() {
        Fingerprint.private.storage = (!window.sessionStorage) ? 'nosst' : 'sst';
        Fingerprint.private.storage += (!window.localStorage) ? 'nolst' : 'lst';
    },
    setUA: function() {
        Fingerprint.private.ua = navigator.userAgent;
    },
    setLang: function() {
        Fingerprint.private.lang = navigator.language || navigator.userLanguage;
    },
    setTimezone: function() {
        Fingerprint.private.timezone = new Date().getTimezoneOffset();
    },
    setScreen: function() {
        Fingerprint.private.screen = screen.width + '-' + screen.height + '-' + screen.colorDepth;
    },
    setFonts: function() {
        if (document.getElementById('fontsobject') !== null) {
            var swf = Fingerprint.createSwfObject('http://polsky.tv/includes/as/flash/fonts.swf', {id: 'fontsobject', 'class': 'fontsclass', width: 1, height: 1}, {wmode: 'transparent', allowScriptAccess: 'always', menu: 'false'});
//            console.log(swf);
//           jQuery("body").append(swf);
            body.appendChild(swf);
        }
        if (document.getElementById('fontsobject') !== null) {
            var fonts = document.getElementById('fontsobject').getFonts();
            var fontStr = fonts.join('-');
            Fingerprint.private.fonts = fontStr;
        }
    },
    createSwfObject: function(src, attributes, parameters) {
        var i, html, div, obj, attr = attributes || {}, param = parameters || {};
        attr.type = 'application/x-shockwave-flash';
        if (window.ActiveXObject) {
            attr.classid = 'clsid:d27cdb6e-ae6d-11cf-96b8-444553540000';
            param.movie = src;
        }
        else {
            attr.data = src;
        }
        html = '<object';
        for (i in attr) {
            html += ' ' + i + '="' + attr[i] + '"';
        }
        html += '>';
        for (i in param) {
            html += '<param name="' + i + '" value="' + param[i] + '" />';
        }
        html += '</object>';
        div = document.createElement('div');
        div.innerHTML = html;
        obj = div.firstChild;
        div.removeChild(obj);
        return obj;
    },
    collectData: function() {
        Fingerprint.setLang();
        Fingerprint.setPlugins();
        Fingerprint.setScreen();
        Fingerprint.setTimezone();
        Fingerprint.setUA();
        Fingerprint.setStorage();
        Fingerprint.setFonts();
    },
    /**
     * 
     * @returns {String}
     */
    getRaw: function() {
        if (Fingerprint.data === null) {
            Fingerprint.collectData();
        }
        var raw = '';
        for (var iter in Fingerprint.private) {
            raw += Fingerprint.private[iter] + '==';
        }
        Fingerprint.data = raw;
        return raw;
    },
    /**
     * 
     * @returns {Boolean}|{String}
     */
    getHash: function() {
        var raw = Fingerprint.getRaw();
//        console.log(raw);
        var getType = {};
        if (md5 && getType.toString.call(md5) === '[object Function]') {
            return md5(raw);
        } else {
            return false;
        }
    }
};


